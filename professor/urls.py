from django.urls import path, include, re_path
from .views import *
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path('scrape', ScrapingAPIView.as_view()),
    path('insert',InsertLocal.as_view()),
    path('scrape2', ScrapingAPIView2.as_view()),
    path('scrape3', ScrapingAPIViewIEEE.as_view()),
    path('create', ProfessorsCreateAPIView.as_view()),
    path('scrape4', ScrapingAPIView4.as_view()),
    path('count_professors', ProfessorsCountView.as_view()),
    path('get_research_areas', ProfessorGetListResearchArea.as_view()),

    path('insert_keyword', InsertInitKeyword2.as_view()),
    path('get_contain_keyword', GetKeywordContains.as_view()),
    path('insert_cat', InsertInitKeyword3.as_view()),
    path('category_alphabet', CategoryAlphabetList.as_view()),
    path('count_prof', ProffesorYab.as_view()),
    path('application', CreateApplications.as_view()),
    path('redirect_bank', GotoVisachanceDargah.as_view()),
    path('result_payment', GotoVisachanceResultPayment.as_view())


]

# urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

