import os , sys
import bibtexparser
from bibtexparser.bibdatabase import as_text
from bibtexparser.bparser import BibTexParser
from django.shortcuts import render
from pybtex.database import parse_string
from rest_framework import generics
from rest_framework.generics import RetrieveAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from .models import *
import requests

def my_scheduled_job():
    try:
        scrap = Scrape.objects.filter(status='None').latest('created_at')
        print(scrap)
        dic = {}
        count = round(scrap.count / 500)
        if count < 1000:
            count = 2
    except Scrape.DoesNotExist:
        # print('unexist')
        # return Response({"message": "success", "status": "OK", "data": "dic"}, status=200)
        return False

    try:
        for i in range(1, count):

            url = "https://www.webofscience.com/api/wosnx/indic/export/saveToFile"
            from_n = i * 500 + 1
            to_n = 500 + i * 500
            qid = str(scrap.qid)
            stid = str(scrap.stid)
            payload = "{\"parentQid\":\"" \
                      + qid + "\",\"sortBy\":\"relevance\",\"displayTimesCited\":\"true\",\"displayCitedRefs\":\"true\",\"product\":\"UA\",\"colName\":\"WOS\",\"displayUsageInfo\":\"true\",\"fileOpt\":\"othersoftware\",\"action\":\"saveToBibtex\",\"markFrom\":\"" \
                      + str(from_n) + "\",\"markTo\":\"" \
                      + str(
                to_n) + "\",\"view\":\"summary\",\"isRefQuery\":\"false\",\"locale\":\"en-US\",\"filters\":\"fullRecordPlus\",\"bm-telemetry\":\"7a74G7m23Vrp0o5c9205991.7-1,2,-94,-100,Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Mobile Safari/537.36,uaend,12147,20030107,en-GB,Gecko,0,0,0,0,403366,2926931,1021,801,1021,801,1021,801,1021,,cpen:0,i1:0,dm:0,cwen:0,non:1,opc:0,fc:0,sc:0,wrc:1,isc:0,vib:1,bat:1,x11:0,x12:1,10173,0.381321889190,819691564747.5,0,loc:-1,2,-94,-101,do_en,dm_en,t_en-1,2,-94,-105,-1,2,-94,-102,0,-1,1,0,1024,-1,0;0,-1,1,0,1025,-1,0;0,-1,0,0,-1,1004,0;0,-1,1,0,1328,-1,0;0,-1,1,0,1650,-1,0;0,-1,0,0,2108,2108,0;-1,2,-94,-108,0,1,56648,8,0,0,1025;1,2,56736,8,0,0,1025;2,1,56815,8,0,0,1025;3,2,56911,8,0,0,1025;4,1,57401,-3,0,0,1025;5,2,57480,-3,0,0,1025;6,1,57504,-2,0,0,1025;7,3,57505,-2,0,0,1025;8,2,57591,-2,0,0,1025;-1,2,-94,-110,0,1,47428,687,375;1,3,47428,687,375,-1;2,4,47439,687,375,-1;3,2,47439,687,375,-1;4,1,52882,676,572;5,3,52884,676,572,2111;6,4,52900,676,572,2111;7,2,52900,676,572,2111;8,1,54276,343,483;9,3,54277,343,483,-1;10,4,54291,343,483,-1;11,2,54291,343,483,-1;12,2,54293,343,483,1595;13,1,54620,353,494;14,3,54621,353,494,-1;15,4,54646,353,494,-1;16,2,54646,353,494,-1;17,2,54653,353,494,1595;18,1,55323,573,497;19,3,55323,573,497,1025;20,4,55342,573,497,1025;21,2,55342,573,497,1025;22,1,55491,573,497;23,3,55491,573,497,1025;24,4,55499,573,497,1025;25,2,55499,573,497,1025;26,1,58876,433,603;27,3,58877,433,603,-1;28,4,58899,433,603,-1;29,2,58899,433,603,-1;30,1,60064,402,710;31,3,60064,402,710,-1;32,4,60084,402,710,-1;33,2,60084,402,710,-1;34,1,60609,384,673;35,3,60609,384,673,-1;36,4,60618,384,673,-1;37,2,60618,384,673,-1;-1,2,-94,-117,0,2,47361,-1,-1;1,3,47425,-1,-1;2,2,52798,-1,-1;3,3,52875,-1,-1;4,2,54190,-1,-1;5,3,54268,-1,-1;6,2,54534,-1,-1;7,3,54611,-1,-1;8,2,55246,-1,-1;9,3,55315,-1,-1;10,2,55407,-1,-1;11,3,55490,-1,-1;12,2,58810,-1,-1;13,3,58874,-1,-1;14,2,59970,-1,-1;15,3,60059,-1,-1;16,2,60532,-1,-1;17,3,60603,-1,-1;-1,2,-94,-111,-1,2,-94,-109,-1,2,-94,-114,0,3,47351,687,375;1,4,47425,688,380;2,3,52788,687,571;3,4,52875,687,571;4,3,54180,332,473;5,4,54267,334,473;6,3,54525,360,503;7,4,54611,360,503;8,3,55235,573,497;9,4,55314,573,497;10,3,55397,573,497;11,4,55489,573,497;12,3,58799,433,603;-1,2,-94,-103,2,42928;3,47344;-1,2,-94,-112,https://www.webofscience.com/wos/woscc/summary/2ce0c838-4f3a-41a6-a81a-40bc1c924c1e-18c2d761/relevance/1(overlay:export/exbt)-1,2,-94,-115,523888,2147339,998562,0,0,711679,4381403,60625,0,1639383129495,154,17537,9,38,2922,18,18,60626,17801188,0,7EBF0F52141C8E358334D83AEEBECFFA~0~YAAQb5lkXz2Iyp59AQAAoXGJsgdggHcsfncweGxER0FbjA2sg+hHb+ZSDWBncAQCzsRkEfSC1whuxF9o5OK539dqQEHitClfEKoA6tYpq2+sJ50fWI0kWHygYpnxCOXlg0cDSM/hHdGb8FdeKNBuvUZrCHhdHOlVCoOS8zvs6TuZBweoZ3bnnON27KXcIbaxxRYGPoNro5Ks5R25gZJfjIwVklvJul2/XClzOdmry9aGvwkZBvbGlzOVTKzPxkiyezFoNCEDJwawwH7+hEVL1keMe9KiwbyDTyS6VFSWCZiNIiPuVplDQ1OWfI+LBl0c6dD0eCou2xqj8VJJpgluMzcoD09Par/uYNT9gO7tbPro2BC54p6MuGOAfOdnusz9u0dyQmqWLkRnH1Ll/6jkllL+o8imKhWQP4CcIF4=~-1~-1~-1,39856,214,1080341895,30261689,PiZtE,60478,106,0,-1-1,2,-94,-106,1,0-1,2,-94,-119,151,159,190,186,260,259,166,159,149,126,123,129,157,956,-1,2,-94,-122,0,0,0,0,1,0,0-1,2,-94,-123,-1,2,-94,-124,-1,2,-94,-126,-1,2,-94,-127,-1,2,-94,-70,-705080415;1993109966;dis;,7,8;true;true;true;-210;true;24;24;true;false;-1-1,2,-94,-80,5579-1,2,-94,-116,8780787-1,2,-94,-118,185099-1,2,-94,-129,-1,2,-94,-121,;3;41;0\"}"

            headers = {
                'authority': 'www.webofscience.com',
                'x-sec-clge-req-type': 'ajax',
                'accept': 'application/json, text/plain, */*',
                'x-1p-wos-sid': stid,
                'user-agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Mobile Safari/537.36',
                'content-type': 'application/json',
                'origin': 'https://www.webofscience.com',
                'sec-fetch-site': 'same-origin',
                'sec-fetch-mode': 'cors',
                'sec-fetch-dest': 'empty',
                'referer': 'https://www.webofscience.com/wos/woscc/summary/2ce0c838-4f3a-41a6-a81a-40bc1c924c1e-18c2d761/relevance/1(overlay:export/exbt)',
                'accept-language': 'en-GB,en-US;q=0.9,en;q=0.8',
                'cookie': 'dotmatics.elementalKey=SLsLWlMhrHnTjDerSrlG; OptanonAlertBoxClosed=2021-12-11T08:51:46.288Z; bm_sz=C175FC987B53372BB52F87D8DBCF256B~YAAQb5lkXzyIyp59AQAAoXGJsg5V72mnzpsbaRsZ+K46rUXYqyD2kAef3bDnsUnPGzAfETlnVuiIgKcLJRYCex6vDFziYPfoBAk8gurqoK3JKZ+MxuLvXZ//nNqOlhyikx63TXXhRt2h8fS6NOkEWC0CYMj7RlKLtBCEIqjVAoY1OZ0fxXTYDGvKTIDOnH8RUK6StG7H; _abck=7EBF0F52141C8E358334D83AEEBECFFA~0~YAAQb5lkXz2Iyp59AQAAoXGJsgdggHcsfncweGxER0FbjA2sg+hHb+ZSDWBncAQCzsRkEfSC1whuxF9o5OK539dqQEHitClfEKoA6tYpq2+sJ50fWI0kWHygYpnxCOXlg0cDSM/hHdGb8FdeKNBuvUZrCHhdHOlVCoOS8zvs6TuZBweoZ3bnnON27KXcIbaxxRYGPoNro5Ks5R25gZJfjIwVklvJul2/XClzOdmry9aGvwkZBvbGlzOVTKzPxkiyezFoNCEDJwawwH7+hEVL1keMe9KiwbyDTyS6VFSWCZiNIiPuVplDQ1OWfI+LBl0c6dD0eCou2xqj8VJJpgluMzcoD09Par/uYNT9gO7tbPro2BC54p6MuGOAfOdnusz9u0dyQmqWLkRnH1Ll/6jkllL+o8imKhWQP4CcIF4=~-1~-1~-1; _sp_ses.840c=*; RT="z=1&dm=www.webofscience.com&si=3922b1fa-17c3-47df-9f5c-ddd0b293faf9&ss=kx4e9jz4&sl=1&tt=5jn&bcn=%2F%2F0217991b.akstat.io%2F&ld=5jr"; OptanonConsent=isIABGlobal=false&datestamp=Mon+Dec+13+2021+11%3A38%3A51+GMT%2B0330+(Iran+Standard+Time)&version=6.25.0&hosts=&consentId=72c7539e-0adc-4434-bbdb-45283e8d2add&interactionCount=1&landingPath=NotLandingPage&groups=C0001%3A1%2CC0002%3A1%2CC0003%3A1&geolocation=GB%3B&AwaitingReconsent=false; bm_sv=7AFA6EB4EFC120A75FD7FD19FC0031E9~gBkrzIabfeMYxLuE3iUVVXTjQiLvtSUguzFdkiJpMvUnfQusdFxHcqKVB30cl0oO1AozAaKTacmoNqdR0IIH6NR0vpdU/+445qGm53vUkE03i6H0YxdcByD/yDTvZw3gzFwqqxhwWz29KbdGldmVJtbx79u40qcZPK+yVqHDTYo=; ak_bmsc=3B57C44F69E578C07CA47B8E180BFDDD~000000000000000000000000000000~YAAQdplkX8aQ6Gx9AQAA6NnWsg6zNdyK8Tqu6aHVyQyXlmcIT3uvEhILPsyLIgrK4JVjlU9cKYhnp8YDDI5h3RF2BUORc5GS3d6v9YiUqO/ABN8tRx3bAg2oJqFll49E5xqTyeceruIqcfYEHFKH8HujYp3OLtMzjOK3FUIr1bc1IvJ6gfrfYBjibXhbfZ4D8xQfen0Guh9+FocwsotpD+5WiJAvXKC0cQcmvrMMTLvWTKvgIgW7YZ8Q6LtWDBO4smeGk3f++xcrGMFq86hoAmchqjZ7U0Ts8ojGALlXzBhxbF8mGyMMEWYrIRqCeB8JhiQdpaL6drOxROGIlH2h9fZWMkEV2sihi/EMF2UD7e6V5Ji+3uUXOvdUskdV3JR7Ja/gG9y816fxtlq6MVIv9PcRdTixXyTHXYaPcw9cvAR7eg==; _sp_id.840c=8090d00c-609f-4e4a-bf4f-408a040c9970.1639212698.7.1639383190.1639378521.46480310-2d1d-4501-a616-067745a030b9; _abck=7EBF0F52141C8E358334D83AEEBECFFA~-1~YAAQdplkXyq/yWx9AQAAoNadrgdLmDzDgiS9a6UTHq0oTsDwFMF3etwvQYNps8Ov08ElmxJQhSYwcRBeTsRsq8jghe2RZpVWIoSIqCicP0SCXmACtzM1mlniuKGLagU1GVn4f658nBGy9gzp8bHd/zKT+0N3q2Frz8uVIJDeghidD6Rpl7fc3c4X/+vuDpwYLabyz0Vqha3TmhKwRtuSwXZ171LVPNibQECphBzYGyEmq7W8uypVJXrAYnhixOihkhDzc4MrAeLxma6JcU014OFzh8z/muvPwDK8s0tXjXbuEjq0KWEALmgOOPZuIilNe0BofM75MswM+aq/Mm4Sks2oSAb/3wdbH/anSsxRqC6b7BXQa75hi/HP1hcdO2t/8GpIT0fyvESrHYXK5faPDk7tdHSBPZiID6/unKp1~-1~-1~-1'
            }

            response = requests.request("POST", url, headers=headers, data=payload)
            data = response.text

            bp = BibTexParser(interpolate_strings=False)
            bib_database = bp.parse(data)

            for input in bib_database.entries:
                db = Professor(da=str(input.get('da')), ui=str(input.get('unique-id')),
                               ga=str(input.get('doc-delivery-number')), u2=str(input.get('usage-count-since-2013')),
                               u1=str(input.get('usage-count-last-180-days')), tc=str(input.get('times-cited')),
                               nr=str(input.get('number-of-cited-references')), cr=str(input.get('cited-references')),
                               wc=str(input.get('web-of-science-categories')), sc=str(input.get('research-areas')),
                               idd=str(input.get('keywords-plus')), bn=str(input.get('isbn')),
                               af=str(input.get('affiliation')), la=str(input.get('language')),
                               dt=str(input.get('type')), add=str(input.get('address')), pu=str(input.get('publisher')),
                               orr=str(input.get('organization')), ntt=str(input.get('note')),
                               pag=str(input.get('pages')), py=str(input.get('year')),
                               bo=str(input.get('booktitle')), ti=str(input.get('title')),
                               au=str(input.get('author')), en=str(input.get('ENTRYTYPE')),
                               idwos=str(input.get('ID')), em=str(input.get('author-email')))

                db.save()

            scrap.note = data[1:1000]
            scrap.status = 'Success'
            scrap.save()
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)
        # print(e)
        scrap.note = str(e)
        scrap.note = 'UnKknow'
        print('unknow')
        scrap.status = 'Fail'
        scrap.save()
