from django_elasticsearch_dsl import Document
from django_elasticsearch_dsl.registries import registry
from .models import Professor

# sudo systemctl enable elasticsearch.service
@registry.register_document
class ProfessorDocument(Document):
    class Index:
        # Name of the Elasticsearch index
        name = 'professor'
        # See Elasticsearch Indices API reference for available settings
        settings = {'number_of_shards': 1,
                    'number_of_replicas': 0}

    class Django:
        model = Professor # The model associated with this Document

        # The fields of the model you want to be indexed in Elasticsearch
        fields = ['fn', 'vr', 'pt', 'au', 'bu', 'bf', 'ca', 'gp', 'be', 'ti', 'so', 'se', 'bs', 'af', 'la', 'dt', 'ct', 'cd', 'cl', 'sp', 'ho', 'de', 'idd', 'ab', 'c1', 'rp', 'em', 'ri', 'oi', 'fu', 'fx', 'cr', 'nr', 'tc', 'z9', 'u1', 'u2', 'pu', 'add', 'pi', 'pa', 'sn', 'ei', 'bn', 'j9', 'j1', 'pd', 'py', 'vl', 'nu', 'pag', 'mo', 'iss', 'si', 'pn', 'su', 'ma', 'bp', 'ep', 'ar', 'di', 'd2', 'ea', 'ey', 'pg', 'p2', 'wc', 'sc', 'ga', 'pm', 'ut', 'oa', 'hp', 'ha', 'da', 'er', 'ef', 'jo', 'ui', 'bo', 'en', 'idwos', 'orr', 'ntt']

        # Ignore auto updating of Elasticsearch when a model is saved
        # or deleted:
        # ignore_signals = True

        # Don't perform an index refresh after every update (overrides global setting):
        # auto_refresh = False

        # Paginate the django queryset used to populate the index with the specified size
        # (by default it uses the database driver's default setting)
        # queryset_pagination = 5000