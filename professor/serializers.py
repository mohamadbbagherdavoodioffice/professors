from rest_framework import serializers
from .models import *

class ProfessorSerializer(serializers.ModelSerializer):

    class Meta:
        model = Professor
        fields = '__all__'


class ProfessorSCSerializer(serializers.ModelSerializer):
    # research area
    class Meta:
        model = Professor
        fields = ['sc']

class CategoryNameSerializer(serializers.ModelSerializer):
    # category name
    class Meta:
        model = CategoryName
        fields = ['name', ]

class TariffNested(serializers.ModelSerializer):
    # category name
    class Meta:
        model = Tariff
        fields = ['name', ]
class ApplicationSerializer(serializers.ModelSerializer):
    read_only_fields = ('application_number')

    # tariff = TariffNested(write_only=True)
    class Meta:
        model = Application
        fields = ['selected_category', 'keywords', 'total_results', 'tariff', 'name', 'cell', 'email', 'town',
                  'application_number']
    def to_representation(self, instance):
        representation = super(ApplicationSerializer, self).to_representation(instance)
        # representation['tariff'] = TariffNested(instance, many=True).data
        return representation