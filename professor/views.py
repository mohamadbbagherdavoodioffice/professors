import json
import sys, os
import bibtexparser
from bibtexparser.bibdatabase import as_text
from bibtexparser.bparser import BibTexParser
from django.db.models import Q
from django.db.models.aggregates import Count
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from pybtex.database import parse_string
from rest_framework import generics
from rest_framework.generics import RetrieveAPIView
from rest_framework.pagination import PageNumberPagination
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView

from sciences.settings import ENDPOINT_PAYMENT
from .models import *
import requests

from .serializers import ProfessorSerializer, ProfessorSCSerializer, CategoryNameSerializer, ApplicationSerializer


class ScrapingAPIView(APIView):
    def get(self, request):
        stid = self.request.GET.get("sid")
        try:
            # scrap = Scrape.objects.filter(status='stid').latest('created_at')
            scrap1 = Scrape.objects.filter(status='qid').order_by('-id')[:100]
            for scrap in scrap1:
                print(scrap.count, scrap.qid, scrap.journal)
                dic = {}

                if scrap.count < 1000:
                    count = 1
                else:
                    count = round(scrap.count / 500)
                print("ssss", count, scrap.journal)

                try:
                    for i in range(0, count):

                        url = "https://www.webofscience.com/api/wosnx/indic/export/saveToFile"
                        from_n = i * 500 + 1
                        to_n = 500 + i * 500
                        qid = str(scrap.qid)
                        print(scrap.qid, "ssss", count, scrap.journal, str(i), from_n, to_n)
                        # D3UfgbE3dHC71jle2Fo
                        # stid = str(scrap.stid)
                        # stid = 'D3UfgbE3dHC71jle2Fo'
                        # payload = "{\"parentQid\":\"" \
                        #           + qid + "\",\"sortBy\":\"relevance\",\"displayTimesCited\":\"true\",\"displayCitedRefs\":\"true\",\"product\":\"UA\",\"colName\":\"WOS\",\"displayUsageInfo\":\"true\",\"fileOpt\":\"othersoftware\",\"action\":\"saveToBibtex\",\"markFrom\":\"" \
                        #           + str(from_n) + "\",\"markTo\":\"" \
                        #           + str(
                        #     to_n) + "\",\"view\":\"summary\",\"isRefQuery\":\"false\",\"locale\":\"en-US\",\"filters\":\"fullRecordPlus\",\"bm-telemetry\":\"7a74G7m23Vrp0o5c9205991.7-1,2,-94,-100,Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Mobile Safari/537.36,uaend,12147,20030107,en-GB,Gecko,0,0,0,0,403366,2926931,1021,801,1021,801,1021,801,1021,,cpen:0,i1:0,dm:0,cwen:0,non:1,opc:0,fc:0,sc:0,wrc:1,isc:0,vib:1,bat:1,x11:0,x12:1,10173,0.381321889190,819691564747.5,0,loc:-1,2,-94,-101,do_en,dm_en,t_en-1,2,-94,-105,-1,2,-94,-102,0,-1,1,0,1024,-1,0;0,-1,1,0,1025,-1,0;0,-1,0,0,-1,1004,0;0,-1,1,0,1328,-1,0;0,-1,1,0,1650,-1,0;0,-1,0,0,2108,2108,0;-1,2,-94,-108,0,1,56648,8,0,0,1025;1,2,56736,8,0,0,1025;2,1,56815,8,0,0,1025;3,2,56911,8,0,0,1025;4,1,57401,-3,0,0,1025;5,2,57480,-3,0,0,1025;6,1,57504,-2,0,0,1025;7,3,57505,-2,0,0,1025;8,2,57591,-2,0,0,1025;-1,2,-94,-110,0,1,47428,687,375;1,3,47428,687,375,-1;2,4,47439,687,375,-1;3,2,47439,687,375,-1;4,1,52882,676,572;5,3,52884,676,572,2111;6,4,52900,676,572,2111;7,2,52900,676,572,2111;8,1,54276,343,483;9,3,54277,343,483,-1;10,4,54291,343,483,-1;11,2,54291,343,483,-1;12,2,54293,343,483,1595;13,1,54620,353,494;14,3,54621,353,494,-1;15,4,54646,353,494,-1;16,2,54646,353,494,-1;17,2,54653,353,494,1595;18,1,55323,573,497;19,3,55323,573,497,1025;20,4,55342,573,497,1025;21,2,55342,573,497,1025;22,1,55491,573,497;23,3,55491,573,497,1025;24,4,55499,573,497,1025;25,2,55499,573,497,1025;26,1,58876,433,603;27,3,58877,433,603,-1;28,4,58899,433,603,-1;29,2,58899,433,603,-1;30,1,60064,402,710;31,3,60064,402,710,-1;32,4,60084,402,710,-1;33,2,60084,402,710,-1;34,1,60609,384,673;35,3,60609,384,673,-1;36,4,60618,384,673,-1;37,2,60618,384,673,-1;-1,2,-94,-117,0,2,47361,-1,-1;1,3,47425,-1,-1;2,2,52798,-1,-1;3,3,52875,-1,-1;4,2,54190,-1,-1;5,3,54268,-1,-1;6,2,54534,-1,-1;7,3,54611,-1,-1;8,2,55246,-1,-1;9,3,55315,-1,-1;10,2,55407,-1,-1;11,3,55490,-1,-1;12,2,58810,-1,-1;13,3,58874,-1,-1;14,2,59970,-1,-1;15,3,60059,-1,-1;16,2,60532,-1,-1;17,3,60603,-1,-1;-1,2,-94,-111,-1,2,-94,-109,-1,2,-94,-114,0,3,47351,687,375;1,4,47425,688,380;2,3,52788,687,571;3,4,52875,687,571;4,3,54180,332,473;5,4,54267,334,473;6,3,54525,360,503;7,4,54611,360,503;8,3,55235,573,497;9,4,55314,573,497;10,3,55397,573,497;11,4,55489,573,497;12,3,58799,433,603;-1,2,-94,-103,2,42928;3,47344;-1,2,-94,-112,https://www.webofscience.com/wos/woscc/summary/2ce0c838-4f3a-41a6-a81a-40bc1c924c1e-18c2d761/relevance/1(overlay:export/exbt)-1,2,-94,-115,523888,2147339,998562,0,0,711679,4381403,60625,0,1639383129495,154,17537,9,38,2922,18,18,60626,17801188,0,7EBF0F52141C8E358334D83AEEBECFFA~0~YAAQb5lkXz2Iyp59AQAAoXGJsgdggHcsfncweGxER0FbjA2sg+hHb+ZSDWBncAQCzsRkEfSC1whuxF9o5OK539dqQEHitClfEKoA6tYpq2+sJ50fWI0kWHygYpnxCOXlg0cDSM/hHdGb8FdeKNBuvUZrCHhdHOlVCoOS8zvs6TuZBweoZ3bnnON27KXcIbaxxRYGPoNro5Ks5R25gZJfjIwVklvJul2/XClzOdmry9aGvwkZBvbGlzOVTKzPxkiyezFoNCEDJwawwH7+hEVL1keMe9KiwbyDTyS6VFSWCZiNIiPuVplDQ1OWfI+LBl0c6dD0eCou2xqj8VJJpgluMzcoD09Par/uYNT9gO7tbPro2BC54p6MuGOAfOdnusz9u0dyQmqWLkRnH1Ll/6jkllL+o8imKhWQP4CcIF4=~-1~-1~-1,39856,214,1080341895,30261689,PiZtE,60478,106,0,-1-1,2,-94,-106,1,0-1,2,-94,-119,151,159,190,186,260,259,166,159,149,126,123,129,157,956,-1,2,-94,-122,0,0,0,0,1,0,0-1,2,-94,-123,-1,2,-94,-124,-1,2,-94,-126,-1,2,-94,-127,-1,2,-94,-70,-705080415;1993109966;dis;,7,8;true;true;true;-210;true;24;24;true;false;-1-1,2,-94,-80,5579-1,2,-94,-116,8780787-1,2,-94,-118,185099-1,2,-94,-129,-1,2,-94,-121,;3;41;0\"}"
                        print(from_n, to_n)
                        payload = "{\"parentQid\":\"" \
                                  + str(
                            qid) + "\",\"sortBy\":\"date-descending\",\"displayTimesCited\":\"true\",\"displayCitedRefs\":\"true\",\"product\":\"UA\",\"colName\":\"WOS\",\"displayUsageInfo\":\"true\",\"fileOpt\":\"othersoftware\",\"action\":\"saveToBibtex\",\"markFrom\":\"" \
                                  + str(from_n) + "\",\"markTo\":\"" \
                                  + str(
                            to_n) + "\",\"view\":\"summary\",\"isRefQuery\":\"false\",\"locale\":\"en-US\",\"filters\":\"fullRecordPlus\",\"bm-telemetry\":\"7a74G7m23Vrp0o5c9205991.7-1,2,-94,-100,Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Mobile Safari/537.36,uaend,12147,20030107,en-GB,Gecko,0,0,0,0,403366,2926931,1021,801,1021,801,1021,801,1021,,cpen:0,i1:0,dm:0,cwen:0,non:1,opc:0,fc:0,sc:0,wrc:1,isc:0,vib:1,bat:1,x11:0,x12:1,10173,0.381321889190,819691564747.5,0,loc:-1,2,-94,-101,do_en,dm_en,t_en-1,2,-94,-105,-1,2,-94,-102,0,-1,1,0,1024,-1,0;0,-1,1,0,1025,-1,0;0,-1,0,0,-1,1004,0;0,-1,1,0,1328,-1,0;0,-1,1,0,1650,-1,0;0,-1,0,0,2108,2108,0;-1,2,-94,-108,0,1,56648,8,0,0,1025;1,2,56736,8,0,0,1025;2,1,56815,8,0,0,1025;3,2,56911,8,0,0,1025;4,1,57401,-3,0,0,1025;5,2,57480,-3,0,0,1025;6,1,57504,-2,0,0,1025;7,3,57505,-2,0,0,1025;8,2,57591,-2,0,0,1025;-1,2,-94,-110,0,1,47428,687,375;1,3,47428,687,375,-1;2,4,47439,687,375,-1;3,2,47439,687,375,-1;4,1,52882,676,572;5,3,52884,676,572,2111;6,4,52900,676,572,2111;7,2,52900,676,572,2111;8,1,54276,343,483;9,3,54277,343,483,-1;10,4,54291,343,483,-1;11,2,54291,343,483,-1;12,2,54293,343,483,1595;13,1,54620,353,494;14,3,54621,353,494,-1;15,4,54646,353,494,-1;16,2,54646,353,494,-1;17,2,54653,353,494,1595;18,1,55323,573,497;19,3,55323,573,497,1025;20,4,55342,573,497,1025;21,2,55342,573,497,1025;22,1,55491,573,497;23,3,55491,573,497,1025;24,4,55499,573,497,1025;25,2,55499,573,497,1025;26,1,58876,433,603;27,3,58877,433,603,-1;28,4,58899,433,603,-1;29,2,58899,433,603,-1;30,1,60064,402,710;31,3,60064,402,710,-1;32,4,60084,402,710,-1;33,2,60084,402,710,-1;34,1,60609,384,673;35,3,60609,384,673,-1;36,4,60618,384,673,-1;37,2,60618,384,673,-1;-1,2,-94,-117,0,2,47361,-1,-1;1,3,47425,-1,-1;2,2,52798,-1,-1;3,3,52875,-1,-1;4,2,54190,-1,-1;5,3,54268,-1,-1;6,2,54534,-1,-1;7,3,54611,-1,-1;8,2,55246,-1,-1;9,3,55315,-1,-1;10,2,55407,-1,-1;11,3,55490,-1,-1;12,2,58810,-1,-1;13,3,58874,-1,-1;14,2,59970,-1,-1;15,3,60059,-1,-1;16,2,60532,-1,-1;17,3,60603,-1,-1;-1,2,-94,-111,-1,2,-94,-109,-1,2,-94,-114,0,3,47351,687,375;1,4,47425,688,380;2,3,52788,687,571;3,4,52875,687,571;4,3,54180,332,473;5,4,54267,334,473;6,3,54525,360,503;7,4,54611,360,503;8,3,55235,573,497;9,4,55314,573,497;10,3,55397,573,497;11,4,55489,573,497;12,3,58799,433,603;-1,2,-94,-103,2,42928;3,47344;-1,2,-94,-112,https://www.webofscience.com/wos/woscc/summary/2ce0c838-4f3a-41a6-a81a-40bc1c924c1e-18c2d761/date-descending/1(overlay:export/exbt)-1,2,-94,-115,523888,2147339,998562,0,0,711679,4381403,60625,0,1639383129495,154,17537,9,38,2922,18,18,60626,17801188,0,7EBF0F52141C8E358334D83AEEBECFFA~0~YAAQb5lkXz2Iyp59AQAAoXGJsgdggHcsfncweGxER0FbjA2sg+hHb+ZSDWBncAQCzsRkEfSC1whuxF9o5OK539dqQEHitClfEKoA6tYpq2+sJ50fWI0kWHygYpnxCOXlg0cDSM/hHdGb8FdeKNBuvUZrCHhdHOlVCoOS8zvs6TuZBweoZ3bnnON27KXcIbaxxRYGPoNro5Ks5R25gZJfjIwVklvJul2/XClzOdmry9aGvwkZBvbGlzOVTKzPxkiyezFoNCEDJwawwH7+hEVL1keMe9KiwbyDTyS6VFSWCZiNIiPuVplDQ1OWfI+LBl0c6dD0eCou2xqj8VJJpgluMzcoD09Par/uYNT9gO7tbPro2BC54p6MuGOAfOdnusz9u0dyQmqWLkRnH1Ll/6jkllL+o8imKhWQP4CcIF4=~-1~-1~-1,39856,214,1080341895,30261689,PiZtE,60478,106,0,-1-1,2,-94,-106,1,0-1,2,-94,-119,151,159,190,186,260,259,166,159,149,126,123,129,157,956,-1,2,-94,-122,0,0,0,0,1,0,0-1,2,-94,-123,-1,2,-94,-124,-1,2,-94,-126,-1,2,-94,-127,-1,2,-94,-70,-705080415;1993109966;dis;,7,8;true;true;true;-210;true;24;24;true;false;-1-1,2,-94,-80,5579-1,2,-94,-116,8780787-1,2,-94,-118,185099-1,2,-94,-129,-1,2,-94,-121,;3;41;0\"}"
                        # print(payload)
                        headers = {
                            'authority': 'www.webofscience.com',
                            'x-sec-clge-req-type': 'ajax',
                            'accept': 'application/json, text/plain, */*',
                            'x-1p-wos-sid': str(stid),
                            'user-agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Mobile Safari/537.36',
                            'content-type': 'application/json',
                            'origin': 'https://www.webofscience.com',
                            'sec-fetch-site': 'same-origin',
                            'sec-fetch-mode': 'cors',
                            'sec-fetch-dest': 'empty',
                            'referer': 'https://www.webofscience.com/wos/woscc/summary/'
                                       + str(scrap.qid) + '/date-descending/1(overlay:export/exbt)',
                            'accept-language': 'en-GB,en-US;q=0.9,en;q=0.8',
                            'cookie': 'OptanonAlertBoxClosed=2022-01-02T08:23:14.266Z; dotmatics.elementalKey=SLsLWlMhrHnTjDerSrlG; OptanonConsent=isIABGlobal=false&datestamp=Tue+Jan+18+2022+13%3A23%3A27+GMT%2B0330+(Iran+Standard+Time)&version=6.25.0&hosts=&consentId=8a9a0b98-668a-476c-ae73-d40239f9fd92&interactionCount=1&landingPath=NotLandingPage&groups=C0001%3A1%2CC0002%3A1%2CC0003%3A1&geolocation=DE%3B&AwaitingReconsent=false; bm_sz=69BA2812F0254FB96C7CD7AA6BBE6BC1~YAAQHujIuQe3mHF+AQAAuCO0gA4GqlbScYVa0Rno3RzDdiRJSA9tt9ELnZ0xdC2IMEhEVmj6oX4fDy1sDCgbq5j3XzjqjaboUFIK47g6ORCr3OFkq/0mNCMg7DbJHh5WGbMAlGkbX/z8UhLt5+JUmHI03zeTX6DSqUfXrNc0LMED9Ve7z12zgT7UNuivxYko2X0jRWjB; _abck=90078C922C09A75197E2798D3E7D4655~0~YAAQHujIuQi3mHF+AQAAuCO0gAfcqgp19b/m2+SogRES11v57McCL/v5zdjYig56pcHJErat2YJ2uK9b/JnkFE4TCoj5SKZJrXarlallYha43SjwxFsJcopVMCOdoI9cjPpDU2niGZg3SOhE7bH7qNd2G9N1thoAucDWE/7tvHt5mIITCKWn0z88z2WlC6s78Fj6zX3P+yUomgV7kZXdEkqm3olhsgvCtOXahruzz2ByDO9YUM5RzW0UzHLgRDY3M9gejPprGWVo/Y5unIEYRaSlLhBJCLhPRmwneSyYfmfu7BcJlqEd/qGgHXZtsiKtCUMeHGZknbTQetoEFVMW2XyAcg9vwI2K+uzRiy32glhcIJpKQkfvGCDf9YaBjoz4eth6m1+u0AlENXJcREobDjvYzTk5cmI1NfEhl0A=~-1~-1~-1; ak_bmsc=8D5D1B44969F71B4DA4FA8A864543C9A~000000000000000000000000000000~YAAQHujIuQm3mHF+AQAAuCO0gA5s0c00FdDZI8tnHK0aIuG7kk/Y2z/EZFdjRv/V9Q7YiJYzeHz39A+JnCxC/3uXay/FBKNH93Kgt5c4rJWvnG4M4Dey1KjkMJx4f3IKEs860eHHBJ+tHBHKwfezp5GUxlSy0e8ISHZnq1h7J4DvZDUWrDtLC+STg4BeubfLgF1k3QXspqoSTQ0bpacGN9/Lz3Wka0J6j8OYyvdalcrQAEUarB4lHwSBk4z1KiIo8dhT42vjYFjqoetDlQDFv2DiUwdXKqceKwSCWVGNmnj2Ab/28WOQ8LvRb+p1F0eGUEpDVvZG67WN8o+WgShVU7GpOzaqWBGj7z45mpTOtDTg9Y21xESXtwmyu0e+DXC0tjVB6TyjCSlIpqNPjy8koA==; RT="z=1&dm=www.webofscience.com&si=7d893c61-695f-401c-b6be-384b72c67543&ss=kypl8zv6&sl=0&tt=0"; _sp_ses.840c=*; bm_sv=0FF4DA10CFB343FF99D46909C8A77F09~E+K1Emjqpb8W462eO1MIR/bUd0C2QZ/s18rCJ/hZYQKN+W1rvUs1GCfc60cQegB5I/AAfuAGS0htR6yGJcg+EDX0I+orklzySv+iSD93savl7eiuw3U//yky7pEm81m2Tw4X7EBf0ha7ioopOw+BU/LlOOerpwY/aY/UcURoTIE=; _sp_id.840c=57d8bf0c-4e4e-414d-adb7-e7a7f6a8843f.1641111784.6.1642842254.1642499862.4144db87-09d9-4f69-8417-a80c4ac8d80c; _abck=643B57EF7A80974BBE3D81D02A8DE4F4~-1~YAAQFd46F/ZX1J58AQAAhOlLpgap9l8PnRNZovL7gxxlR+rkon98Fgv5ImXTO8FsCodYgXK7eHnyLJy94un9kRK38DfxlI2ls06cvXsFb1+f+8m2vnYBhUjxJhpWxOG2aEdsu1s08JPhwZBCdcxuoFmUoJB69L/NZ94oTuXQ2Xo/o4avKV+WHEW+dkcqYVCaq7FNrHq7+F6YZLuX9DiBefqDnqfxX6XTFh6xNDWBAsEgOs/QG5dI06dHvG+O/Rlgn/E3MfB4dJ4beZr1qxAYgOmowHRRaxZBLvPhTtTnYlGUp7TplzPbStC8gQCBH1FofWnGgj73QZcvugelX/h0HdGEYpCDLiogQ7Nvzjo2lRRLS7hosVbPE3xYT+lsnv9RQkkfx4OsQ5FlOeKoKIyLNNx00gjz8kqpTLTr+hc=~0~-1~-1'
                        }


                        response = requests.request("POST", url, headers=headers, data=payload)
                        # print(response.content)
                        data = response.text
                        # print(data)
                        bp = BibTexParser(interpolate_strings=False)
                        bib_database = bp.parse(data)

                        with open('/root/BibFile/' + str(scrap.journal) + "(" + str(i) + ")", 'w') as f:
                            f.write(str(data))
                            f.close()
                        if len(bib_database.entries) <= 10:
                            return Response({"message": "Fail qid:" + str(scrap.qid), "status": "OK", "data": dic},
                                            status=200)

                        for input in bib_database.entries:
                            db = Professor(da=str(input.get('da')), ui=str(input.get('unique-id')),
                                           ga=str(input.get('doc-delivery-number')),
                                           u2=str(input.get('usage-count-since-2013')),
                                           u1=str(input.get('usage-count-last-180-days')),
                                           tc=str(input.get('times-cited')),
                                           nr=str(input.get('number-of-cited-references')),
                                           cr=str(input.get('cited-references')),
                                           wc=str(input.get('web-of-science-categories')),
                                           sc=str(input.get('research-areas')),
                                           idd=str(input.get('keywords-plus')), bn=str(input.get('isbn')),
                                           af=str(input.get('affiliation')), la=str(input.get('language')),
                                           dt=str(input.get('type')), add=str(input.get('address')),
                                           pu=str(input.get('publisher')),
                                           orr=str(input.get('organization')), ntt=str(input.get('note')),
                                           pag=str(input.get('pages')), py=str(input.get('year')),
                                           bo=str(input.get('booktitle')), ti=str(input.get('title')),
                                           au=str(input.get('author')), en=str(input.get('ENTRYTYPE')),
                                           idwos=str(input.get('ID')), em=str(input.get('author-email')),
                                           jo=str(scrap.journal))
                            # print(db,"++++")
                            db.save()

                        scrap.note = data[1:3000]
                        scrap.status = 'Success'
                        scrap.save()
                        dic = ["true"]
                except Exception as e:
                    exc_type, exc_obj, exc_tb = sys.exc_info()
                    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                    print(exc_type, fname, exc_tb.tb_lineno)
                    # print(e)
                    scrap.note = str(exc_type) + ": " + str(fname) + ": " + str(exc_tb.tb_lineno)
                    scrap.status = 'Fail'
                    scrap.save()
                    dic = ["error"]


        except Scrape.DoesNotExist:
            dic = ["error1"]
        # return Response({"message": "success", "status": "OK", "data": "dic"}, status=200)
        # return False
        # print(dic,"||||||||||||||||||")
        return Response({"message": "success", "status": "OK", "data": dic}, status=200)


class ScrapingAPIView2(APIView):
    def get(self, request):
        stid = self.request.GET.get("sid")
        try:
            # scrap = Scrape.objects.filter(status='stid').latest('created_at')
            scrap1 = Scrape.objects.filter(status='qid').order_by('id')[:100]
            for scrap in scrap1:
                print(scrap.count, scrap.qid, scrap.journal)
                dic = {}

                if scrap.count < 1000:
                    count = 1
                else:
                    count = round(scrap.count / 500)
                print("ssss", count, scrap.journal)

                try:
                    for i in range(0, count):

                        url = "https://www.webofscience.com/api/wosnx/indic/export/saveToFile"
                        from_n = i * 500 + 1
                        to_n = 500 + i * 500
                        qid = str(scrap.qid)
                        print(scrap.qid, "ssss", count, scrap.journal, str(i), from_n, to_n)
                        # D3UfgbE3dHC71jle2Fo
                        # stid = str(scrap.stid)
                        # stid = 'D3UfgbE3dHC71jle2Fo'
                        # payload = "{\"parentQid\":\"" \
                        #           + qid + "\",\"sortBy\":\"relevance\",\"displayTimesCited\":\"true\",\"displayCitedRefs\":\"true\",\"product\":\"UA\",\"colName\":\"WOS\",\"displayUsageInfo\":\"true\",\"fileOpt\":\"othersoftware\",\"action\":\"saveToBibtex\",\"markFrom\":\"" \
                        #           + str(from_n) + "\",\"markTo\":\"" \
                        #           + str(
                        #     to_n) + "\",\"view\":\"summary\",\"isRefQuery\":\"false\",\"locale\":\"en-US\",\"filters\":\"fullRecordPlus\",\"bm-telemetry\":\"7a74G7m23Vrp0o5c9205991.7-1,2,-94,-100,Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Mobile Safari/537.36,uaend,12147,20030107,en-GB,Gecko,0,0,0,0,403366,2926931,1021,801,1021,801,1021,801,1021,,cpen:0,i1:0,dm:0,cwen:0,non:1,opc:0,fc:0,sc:0,wrc:1,isc:0,vib:1,bat:1,x11:0,x12:1,10173,0.381321889190,819691564747.5,0,loc:-1,2,-94,-101,do_en,dm_en,t_en-1,2,-94,-105,-1,2,-94,-102,0,-1,1,0,1024,-1,0;0,-1,1,0,1025,-1,0;0,-1,0,0,-1,1004,0;0,-1,1,0,1328,-1,0;0,-1,1,0,1650,-1,0;0,-1,0,0,2108,2108,0;-1,2,-94,-108,0,1,56648,8,0,0,1025;1,2,56736,8,0,0,1025;2,1,56815,8,0,0,1025;3,2,56911,8,0,0,1025;4,1,57401,-3,0,0,1025;5,2,57480,-3,0,0,1025;6,1,57504,-2,0,0,1025;7,3,57505,-2,0,0,1025;8,2,57591,-2,0,0,1025;-1,2,-94,-110,0,1,47428,687,375;1,3,47428,687,375,-1;2,4,47439,687,375,-1;3,2,47439,687,375,-1;4,1,52882,676,572;5,3,52884,676,572,2111;6,4,52900,676,572,2111;7,2,52900,676,572,2111;8,1,54276,343,483;9,3,54277,343,483,-1;10,4,54291,343,483,-1;11,2,54291,343,483,-1;12,2,54293,343,483,1595;13,1,54620,353,494;14,3,54621,353,494,-1;15,4,54646,353,494,-1;16,2,54646,353,494,-1;17,2,54653,353,494,1595;18,1,55323,573,497;19,3,55323,573,497,1025;20,4,55342,573,497,1025;21,2,55342,573,497,1025;22,1,55491,573,497;23,3,55491,573,497,1025;24,4,55499,573,497,1025;25,2,55499,573,497,1025;26,1,58876,433,603;27,3,58877,433,603,-1;28,4,58899,433,603,-1;29,2,58899,433,603,-1;30,1,60064,402,710;31,3,60064,402,710,-1;32,4,60084,402,710,-1;33,2,60084,402,710,-1;34,1,60609,384,673;35,3,60609,384,673,-1;36,4,60618,384,673,-1;37,2,60618,384,673,-1;-1,2,-94,-117,0,2,47361,-1,-1;1,3,47425,-1,-1;2,2,52798,-1,-1;3,3,52875,-1,-1;4,2,54190,-1,-1;5,3,54268,-1,-1;6,2,54534,-1,-1;7,3,54611,-1,-1;8,2,55246,-1,-1;9,3,55315,-1,-1;10,2,55407,-1,-1;11,3,55490,-1,-1;12,2,58810,-1,-1;13,3,58874,-1,-1;14,2,59970,-1,-1;15,3,60059,-1,-1;16,2,60532,-1,-1;17,3,60603,-1,-1;-1,2,-94,-111,-1,2,-94,-109,-1,2,-94,-114,0,3,47351,687,375;1,4,47425,688,380;2,3,52788,687,571;3,4,52875,687,571;4,3,54180,332,473;5,4,54267,334,473;6,3,54525,360,503;7,4,54611,360,503;8,3,55235,573,497;9,4,55314,573,497;10,3,55397,573,497;11,4,55489,573,497;12,3,58799,433,603;-1,2,-94,-103,2,42928;3,47344;-1,2,-94,-112,https://www.webofscience.com/wos/woscc/summary/2ce0c838-4f3a-41a6-a81a-40bc1c924c1e-18c2d761/relevance/1(overlay:export/exbt)-1,2,-94,-115,523888,2147339,998562,0,0,711679,4381403,60625,0,1639383129495,154,17537,9,38,2922,18,18,60626,17801188,0,7EBF0F52141C8E358334D83AEEBECFFA~0~YAAQb5lkXz2Iyp59AQAAoXGJsgdggHcsfncweGxER0FbjA2sg+hHb+ZSDWBncAQCzsRkEfSC1whuxF9o5OK539dqQEHitClfEKoA6tYpq2+sJ50fWI0kWHygYpnxCOXlg0cDSM/hHdGb8FdeKNBuvUZrCHhdHOlVCoOS8zvs6TuZBweoZ3bnnON27KXcIbaxxRYGPoNro5Ks5R25gZJfjIwVklvJul2/XClzOdmry9aGvwkZBvbGlzOVTKzPxkiyezFoNCEDJwawwH7+hEVL1keMe9KiwbyDTyS6VFSWCZiNIiPuVplDQ1OWfI+LBl0c6dD0eCou2xqj8VJJpgluMzcoD09Par/uYNT9gO7tbPro2BC54p6MuGOAfOdnusz9u0dyQmqWLkRnH1Ll/6jkllL+o8imKhWQP4CcIF4=~-1~-1~-1,39856,214,1080341895,30261689,PiZtE,60478,106,0,-1-1,2,-94,-106,1,0-1,2,-94,-119,151,159,190,186,260,259,166,159,149,126,123,129,157,956,-1,2,-94,-122,0,0,0,0,1,0,0-1,2,-94,-123,-1,2,-94,-124,-1,2,-94,-126,-1,2,-94,-127,-1,2,-94,-70,-705080415;1993109966;dis;,7,8;true;true;true;-210;true;24;24;true;false;-1-1,2,-94,-80,5579-1,2,-94,-116,8780787-1,2,-94,-118,185099-1,2,-94,-129,-1,2,-94,-121,;3;41;0\"}"
                        print(from_n, to_n)
                        payload = "{\"parentQid\":\"" \
                                  + str(
                            qid) + "\",\"sortBy\":\"date-descending\",\"displayTimesCited\":\"true\",\"displayCitedRefs\":\"true\",\"product\":\"UA\",\"colName\":\"WOS\",\"displayUsageInfo\":\"true\",\"fileOpt\":\"othersoftware\",\"action\":\"saveToBibtex\",\"markFrom\":\"" \
                                  + str(from_n) + "\",\"markTo\":\"" \
                                  + str(
                            to_n) + "\",\"view\":\"summary\",\"isRefQuery\":\"false\",\"locale\":\"en-US\",\"filters\":\"fullRecordPlus\",\"bm-telemetry\":\"7a74G7m23Vrp0o5c9205991.7-1,2,-94,-100,Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Mobile Safari/537.36,uaend,12147,20030107,en-GB,Gecko,0,0,0,0,403366,2926931,1021,801,1021,801,1021,801,1021,,cpen:0,i1:0,dm:0,cwen:0,non:1,opc:0,fc:0,sc:0,wrc:1,isc:0,vib:1,bat:1,x11:0,x12:1,10173,0.381321889190,819691564747.5,0,loc:-1,2,-94,-101,do_en,dm_en,t_en-1,2,-94,-105,-1,2,-94,-102,0,-1,1,0,1024,-1,0;0,-1,1,0,1025,-1,0;0,-1,0,0,-1,1004,0;0,-1,1,0,1328,-1,0;0,-1,1,0,1650,-1,0;0,-1,0,0,2108,2108,0;-1,2,-94,-108,0,1,56648,8,0,0,1025;1,2,56736,8,0,0,1025;2,1,56815,8,0,0,1025;3,2,56911,8,0,0,1025;4,1,57401,-3,0,0,1025;5,2,57480,-3,0,0,1025;6,1,57504,-2,0,0,1025;7,3,57505,-2,0,0,1025;8,2,57591,-2,0,0,1025;-1,2,-94,-110,0,1,47428,687,375;1,3,47428,687,375,-1;2,4,47439,687,375,-1;3,2,47439,687,375,-1;4,1,52882,676,572;5,3,52884,676,572,2111;6,4,52900,676,572,2111;7,2,52900,676,572,2111;8,1,54276,343,483;9,3,54277,343,483,-1;10,4,54291,343,483,-1;11,2,54291,343,483,-1;12,2,54293,343,483,1595;13,1,54620,353,494;14,3,54621,353,494,-1;15,4,54646,353,494,-1;16,2,54646,353,494,-1;17,2,54653,353,494,1595;18,1,55323,573,497;19,3,55323,573,497,1025;20,4,55342,573,497,1025;21,2,55342,573,497,1025;22,1,55491,573,497;23,3,55491,573,497,1025;24,4,55499,573,497,1025;25,2,55499,573,497,1025;26,1,58876,433,603;27,3,58877,433,603,-1;28,4,58899,433,603,-1;29,2,58899,433,603,-1;30,1,60064,402,710;31,3,60064,402,710,-1;32,4,60084,402,710,-1;33,2,60084,402,710,-1;34,1,60609,384,673;35,3,60609,384,673,-1;36,4,60618,384,673,-1;37,2,60618,384,673,-1;-1,2,-94,-117,0,2,47361,-1,-1;1,3,47425,-1,-1;2,2,52798,-1,-1;3,3,52875,-1,-1;4,2,54190,-1,-1;5,3,54268,-1,-1;6,2,54534,-1,-1;7,3,54611,-1,-1;8,2,55246,-1,-1;9,3,55315,-1,-1;10,2,55407,-1,-1;11,3,55490,-1,-1;12,2,58810,-1,-1;13,3,58874,-1,-1;14,2,59970,-1,-1;15,3,60059,-1,-1;16,2,60532,-1,-1;17,3,60603,-1,-1;-1,2,-94,-111,-1,2,-94,-109,-1,2,-94,-114,0,3,47351,687,375;1,4,47425,688,380;2,3,52788,687,571;3,4,52875,687,571;4,3,54180,332,473;5,4,54267,334,473;6,3,54525,360,503;7,4,54611,360,503;8,3,55235,573,497;9,4,55314,573,497;10,3,55397,573,497;11,4,55489,573,497;12,3,58799,433,603;-1,2,-94,-103,2,42928;3,47344;-1,2,-94,-112,https://www.webofscience.com/wos/woscc/summary/2ce0c838-4f3a-41a6-a81a-40bc1c924c1e-18c2d761/date-descending/1(overlay:export/exbt)-1,2,-94,-115,523888,2147339,998562,0,0,711679,4381403,60625,0,1639383129495,154,17537,9,38,2922,18,18,60626,17801188,0,7EBF0F52141C8E358334D83AEEBECFFA~0~YAAQb5lkXz2Iyp59AQAAoXGJsgdggHcsfncweGxER0FbjA2sg+hHb+ZSDWBncAQCzsRkEfSC1whuxF9o5OK539dqQEHitClfEKoA6tYpq2+sJ50fWI0kWHygYpnxCOXlg0cDSM/hHdGb8FdeKNBuvUZrCHhdHOlVCoOS8zvs6TuZBweoZ3bnnON27KXcIbaxxRYGPoNro5Ks5R25gZJfjIwVklvJul2/XClzOdmry9aGvwkZBvbGlzOVTKzPxkiyezFoNCEDJwawwH7+hEVL1keMe9KiwbyDTyS6VFSWCZiNIiPuVplDQ1OWfI+LBl0c6dD0eCou2xqj8VJJpgluMzcoD09Par/uYNT9gO7tbPro2BC54p6MuGOAfOdnusz9u0dyQmqWLkRnH1Ll/6jkllL+o8imKhWQP4CcIF4=~-1~-1~-1,39856,214,1080341895,30261689,PiZtE,60478,106,0,-1-1,2,-94,-106,1,0-1,2,-94,-119,151,159,190,186,260,259,166,159,149,126,123,129,157,956,-1,2,-94,-122,0,0,0,0,1,0,0-1,2,-94,-123,-1,2,-94,-124,-1,2,-94,-126,-1,2,-94,-127,-1,2,-94,-70,-705080415;1993109966;dis;,7,8;true;true;true;-210;true;24;24;true;false;-1-1,2,-94,-80,5579-1,2,-94,-116,8780787-1,2,-94,-118,185099-1,2,-94,-129,-1,2,-94,-121,;3;41;0\"}"
                        # print(payload)
                        headers = {
                            'authority': 'www.webofscience.com',
                            'x-sec-clge-req-type': 'ajax',
                            'accept': 'application/json, text/plain, */*',
                            'x-1p-wos-sid': str(stid),
                            'user-agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Mobile Safari/537.36',
                            'content-type': 'application/json',
                            'origin': 'https://www.webofscience.com',
                            'sec-fetch-site': 'same-origin',
                            'sec-fetch-mode': 'cors',
                            'sec-fetch-dest': 'empty',
                            'referer': 'https://www.webofscience.com/wos/woscc/summary/2ce0c838-4f3a-41a6-a81a-40bc1c924c1e-18c2d761/date-descending/1(overlay:export/exbt)',
                            'accept-language': 'en-GB,en-US;q=0.9,en;q=0.8',
                            'cookie': 'AMCV_8E929CC25A1FB2B30A495C97%40AdobeOrg=1687686476%7CMCIDTS%7C19004%7CMCMID%7C83841581533304564870017784049106444557%7CMCAAMLH-1642504341%7C6%7CMCAAMB-1642504341%7CRKhpRz8krg2tLO6pguXWp5olkAcUniQYPHaMWWgdJ3xzPWQmdj0y%7CMCOPTOUT-1641906741s%7CNONE%7CMCAID%7CNONE%7CvVersion%7C3.0.0; fp=3592e1dd994779bad8447aa70c1d9ec2; __gads=ID=fde503867c2a57df:T=1641899555:S=ALNI_Mbu0bpdhk1bpUVEEs-bdSzCIcS-0A; cookieconsent_status=dismiss; ezproxy=U21RwMCVucndkMg; ezproxyl=U21RwMCVucndkMg; ezproxyn=U21RwMCVucndkMg; utag_main=v_id:017e48d6bbdc005c94cf0609c30c00087004f07f0085c$_sn:6$_se:1$_ss:1$_st:1642327433411$vapi_domain:usantotomas.edu.co$ses_id:1642325633411%3Bexp-session$_pn:1%3Bexp-session'
                        }

                        print("step1")
                        response = requests.request("POST", url, headers=headers, data=payload)
                        # print(response.content)
                        data = response.text
                        # print(data)
                        bp = BibTexParser(interpolate_strings=False)
                        bib_database = bp.parse(data)

                        with open('/root/BibFile/' + str(scrap.journal) + "(" + str(i) + ")", 'w') as f:
                            f.write(str(data))
                            f.close()
                        if len(bib_database.entries) <= 10:
                            return Response({"message": "Fail qid:" + str(scrap.qid), "status": "OK", "data": dic},
                                            status=200)

                        for input in bib_database.entries:
                            db = Professor(da=str(input.get('da')), ui=str(input.get('unique-id')),
                                           ga=str(input.get('doc-delivery-number')),
                                           u2=str(input.get('usage-count-since-2013')),
                                           u1=str(input.get('usage-count-last-180-days')),
                                           tc=str(input.get('times-cited')),
                                           nr=str(input.get('number-of-cited-references')),
                                           cr=str(input.get('cited-references')),
                                           wc=str(input.get('web-of-science-categories')),
                                           sc=str(input.get('research-areas')),
                                           idd=str(input.get('keywords-plus')), bn=str(input.get('isbn')),
                                           af=str(input.get('affiliation')), la=str(input.get('language')),
                                           dt=str(input.get('type')), add=str(input.get('address')),
                                           pu=str(input.get('publisher')),
                                           orr=str(input.get('organization')), ntt=str(input.get('note')),
                                           pag=str(input.get('pages')), py=str(input.get('year')),
                                           bo=str(input.get('booktitle')), ti=str(input.get('title')),
                                           au=str(input.get('author')), en=str(input.get('ENTRYTYPE')),
                                           idwos=str(input.get('ID')), em=str(input.get('author-email')),
                                           jo=str(scrap.journal))
                            # print(db,"++++")
                            db.save()

                        scrap.note = data[1:3000]
                        scrap.status = 'Success'
                        scrap.save()
                        dic = [qid]
                except Exception as e:
                    exc_type, exc_obj, exc_tb = sys.exc_info()
                    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                    print(exc_type, fname, exc_tb.tb_lineno)
                    # print(e)
                    scrap.note = str(exc_type) + ": " + str(fname) + ": " + str(exc_tb.tb_lineno)
                    scrap.status = 'Fail'
                    scrap.save()
                    dic = ["error"]


        except Scrape.DoesNotExist:
            dic = ["error1"]
        # return Response({"message": "success", "status": "OK", "data": "dic"}, status=200)
        # return False
        # print(dic,"||||||||||||||||||")
        return Response({"message": "success", "status": "OK", "data": dic}, status=200)


class ScrapingAPIViewIEEE(APIView):
    def get(self, request):
        stid = self.request.GET.get("sid")
        try:

            scrap1 = ScrapeIEEE.objects.filter(status='None').order_by('id').last()
            print(scrap1.journal, "ieeee")

            years = ["2012_2012_Year", "2013_2013_Year", "2014_2014_Year",
                     "2015_2015_Year", "2016_2016_Year", "2017_2017_Year"
                                                         "2018_2018_Year", "2019_2019_Year", "2020_2020_Year"
                , "2021_2021_Year", ]
            # years = ["2001_2001_Year"]

            for xyear in years:
                url = "https://ieeexplore-ieee-org.crai-ustadigital.usantotomas.edu.co/rest/search/export-csv"
                payload = "{\"action\":\"search\",\"matchBoolean\":true,\"queryText\":\"('All Metadata':'" \
                          + str(
                    scrap1.journal) + "')\",\"highlight\":true,\"returnType\":\"SEARCH\",\"matchPubs\":true,\"sortType\":\"newest\",\"ranges\":[\"" \
                          + str(xyear) + "\"],\"returnFacets\":[\"ALL\"]}"
                headers = {
                    'Connection': 'keep-alive',
                    'Accept': 'application/json, text/plain, */*',
                    'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Mobile Safari/537.36',
                    'Content-Type': 'application/json',
                    'Origin': 'https://ieeexplore-ieee-org.crai-ustadigital.usantotomas.edu.co',
                    'Sec-Fetch-Site': 'same-origin',
                    'Sec-Fetch-Mode': 'cors',
                    'Sec-Fetch-Dest': 'empty',
                    'Referer': 'https://ieeexplore-ieee-org.crai-ustadigital.usantotomas.edu.co/search/searchresult.jsp?action=search&matchBoolean=true&queryText=(%22All%20Metadata%22:%22Journal%20of%20Petrology%22)&highlight=true&returnFacets=ALL&returnType=SEARCH&matchPubs=true&sortType=newest&ranges=' + str(
                        xyear),
                    'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
                    'Cookie': str(stid)
                }

                response = requests.request("POST", url, headers=headers, data=payload)
                import json
                lines = response.text.split('\n')
                cc = 0
                for line in lines:
                    # ['"Document Title"', 'Authors', '"Author Affiliations"',
                    # '"Publication Title"', 'Date Added To Xplore',
                    # '"Publication Year"', '"Volume"', '"Issue"',
                    # '"Start Page"', '"End Page"', '"Abstract"', '"ISSN"',
                    # 'ISBNs', '"DOI"', 'Funding Information', 'PDF Link',
                    # '"Author Keywords"', '"IEEE Terms"', '"INSPEC Controlled Terms"',
                    # '"INSPEC Non-Controlled Terms"', '"Mesh_Terms"', 'Article Citation Count',
                    # 'Patent Citation Count', '"Reference Count"', '"License"',
                    # 'Online Date', 'Issue Date', '"Meeting Date"', '"Publisher"', 'Document Identifier']
                    if cc != 0:
                        data = line.split('","')
                        # print(data)
                        ti = data[0]
                        au = data[1]
                        af = data[2]
                        pu = data[3]
                        data[4]
                        py = data[5]
                        vl = data[6]
                        iss = data[7]
                        pag = str(data[8]) + "_" + str(data[9])
                        ab = data[10]
                        sn = data[11]
                        data[12]
                        di = data[13]
                        pt = 'IEEE'
                        idd = str(data[16]) + ";" + str(data[17]) + ";" + str(data[18]) + ";" + str(data[19])
                        nr = str(data[20])
                        jo = str(scrap1.journal)
                        tc = str(data[21])
                        # print(data)
                        dbs = Professor(jo=jo, ti=ti, au=au, af=af, pu=pu, py=py, vl=vl, iss=iss, pag=pag, ab=ab, sn=sn,
                                        di=di, pt=pt, idd=idd, nr=nr, tc=tc)
                        sss = dbs.save()
                        print(sn)

                    cc = cc + 1
                    scrap1.status = "Success"
                    scrap1.save()
                    dic = {}
                    dic["data"] = str(scrap1.journal)

                # print(json.dumps(response.text))
                with open(str(scrap1.journal) + "(" + str(xyear) + ")", 'w') as f:
                    f.write(str(response.text))
                    f.close()

        except:
            # dic = ["error1"]
            print(scrap1.journal)
            dic["data"] = "error2"

        return Response({"message": "success", "status": "OK", "data": dic}, status=200)


class ProfessorsCreateAPIView(generics.CreateAPIView):
    queryset = Professor.objects.all()
    serializer_class = ProfessorSerializer


class ScrapingAPIView4(APIView):
    def get(self, request):
        try:
            # folder0 = "/home/bagher/Desktop/sceience2/science/BibFile"
            folder0 = "/home/bagher/Desktop/sceience2/science/BibFile"
            all_files = os.listdir("/home/bagher/Desktop/sceience2/science/BibFile")
            for all in all_files:
                fpath = folder0 + "/" + all
                print(fpath)
                with open(fpath, encoding='utf8') as f:
                    bp = BibTexParser(interpolate_strings=False)
                    bib_database = bp.parse(f.read())
                    for input in bib_database.entries:
                        db = Professor(da=str(input.get('da')), ui=str(input.get('unique-id')),
                                       ga=str(input.get('doc-delivery-number')),
                                       u2=str(input.get('usage-count-since-2013')),
                                       u1=str(input.get('usage-count-last-180-days')),
                                       tc=str(input.get('times-cited')),
                                       nr=str(input.get('number-of-cited-references')),
                                       cr=str(input.get('cited-references')),
                                       wc=str(input.get('web-of-science-categories')),
                                       sc=str(input.get('research-areas')),
                                       idd=str(input.get('keywords-plus')), bn=str(input.get('isbn')),
                                       af=str(input.get('affiliation')), la=str(input.get('language')),
                                       dt=str(input.get('type')), add=str(input.get('address')),
                                       pu=str(input.get('publisher')),
                                       orr=str(input.get('organization')), ntt=str(input.get('note')),
                                       pag=str(input.get('pages')), py=str(input.get('year')),
                                       bo=str(input.get('booktitle')), ti=str(input.get('title')),
                                       au=str(input.get('author')), en=str(input.get('ENTRYTYPE')),
                                       idwos=str(input.get('ID')), em=str(input.get('author-email')),
                                       jo=str(all))
                        # print(db,"++++")
                        dbclean_search = ProfessorCleaned.objects.filter(sc=str(input.get('research-areas')))
                        if len(dbclean_search)<=0 and input.get('author-email') is not None:


                            dbclean = ProfessorCleaned(sc=str(input.get('research-areas'))
                                                         , au=str(input.get('author'))
                                                         , em =str(input.get('author-email')))
                            dbclean.save()
                        db.save()


        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            # print(e)

        return Response({"message": "success", "status": "OK", "data": []}, status=200)


class ProfessorsCountView(APIView):
    """
    A view that returns the count of active professors
    """
    renderer_classes = (JSONRenderer,)

    def get(self, request, format=None):
        user_count = Professor.objects.count()
        content = {'count': user_count}
        return Response(content)


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 100
    page_size_query_param = 'page_size'
    max_page_size = 1000


class ProfessorGetListResearchArea(generics.ListAPIView):
    # print("1111")
    serializer_class = ProfessorSCSerializer

    pagination_class = StandardResultsSetPagination

    def get_queryset(self):
        queryset = Professor.objects.all().exclude(sc__isnull=True).exclude(
            sc__exact=" ").exclude(sc__exact='').exclude(sc__exact="").order_by('sc')
        print(queryset)
        return queryset



class InsertLocal(APIView):
    def get(self, request):
        folder0 = "/home/timnak/Desktop/BIBFILE"
        all_files = os.listdir("/home/timnak/Desktop/BIBFILE")
        aall_files = all_files.sort(reverse=False)
        for all in all_files:
            fpath = folder0 + "/" + all
            print(all)
            # open text file in read mode
            text_file = open(fpath, "r")

            # read whole file to a string
            data = text_file.read()

            # close file
            text_file.close()
            # print(type(data))
            bp = BibTexParser(interpolate_strings=False)
            bib_database = bp.parse(data)
            # for input in bib_database.entries:
            #     da = str(input.get('da'))
            #     print(da)

            if len(bib_database.entries) >= 10:

                for input in bib_database.entries:
                    db = Professor(da=str(input.get('da')), ui=str(input.get('unique-id')),
                                   ga=str(input.get('doc-delivery-number')),
                                   u2=str(input.get('usage-count-since-2013')),
                                   u1=str(input.get('usage-count-last-180-days')),
                                   tc=str(input.get('times-cited')),
                                   nr=str(input.get('number-of-cited-references')),
                                   cr=str(input.get('cited-references')),
                                   wc=str(input.get('web-of-science-categories')),
                                   sc=str(input.get('research-areas')),
                                   idd=str(input.get('keywords-plus')), bn=str(input.get('isbn')),
                                   af=str(input.get('affiliation')), la=str(input.get('language')),
                                   dt=str(input.get('type')), add=str(input.get('address')),
                                   pu=str(input.get('publisher')),
                                   orr=str(input.get('organization')), ntt=str(input.get('note')),
                                   pag=str(input.get('pages')), py=str(input.get('year')),
                                   bo=str(input.get('booktitle')), ti=str(input.get('title')),
                                   au=str(input.get('author')), en=str(input.get('ENTRYTYPE')),
                                   idwos=str(input.get('ID')), em=str(input.get('author-email')),
                                   jo=str(all))
                    db.save()

        return Response({"message": "success", "status": "OK", "data": []}, status=200)


class GetKeywordContains(APIView):
    def get(self, request):
        q = self.request.GET.get("q")
        name = KeywordName.objects.filter(name__contains=q).values_list('name', flat=True)
        return Response({"names": name}, status=200)


class InsertInitKeyword(APIView):
    def get(self, request):

        # KeywordName.objects.all().delete()
        queryset = Professor.objects.all()
        count_sucess = 0
        print(len(queryset))
        for q in queryset:

            # print(q.idd)

            keys = q.idd.split(';')
            # print(keys[0]=='None')
            # print(keys)

            for k in keys:
                if not 'None' in keys[0]:
                    k = k.replace("{", "")
                    k = k.replace("}", "")
                    k = k.replace(".", "")
                    k = k.replace("!", "")
                    k = k.replace("%", "")
                    k = k.replace("$", "")
                    k = k.replace("#", "")
                    k = k.replace("*", "")
                    k = k.strip()
                    k = k.replace("\n", "")
                    k = k.lower()
                    keyso = KeywordName()
                    keyso.name = str(k)
                    keyso.save()
                    count_sucess += 1

        return Response({"success": count_sucess}, status=200)



class InsertInitKeyword2(APIView):
    def get(self, request):

        folder0 = "/home/timnak/Desktop/BIBFILE"
        all_files = os.listdir("/home/timnak/Desktop/BIBFILE")
        aall_files = all_files.sort(reverse=False)
        for all in all_files:
            fpath = folder0 + "/" + all
            print(all)
            # open text file in read mode
            text_file = open(fpath, "r")

            # read whole file to a string
            data = text_file.read()

            # close file
            text_file.close()
            # print(type(data))
            bp = BibTexParser(interpolate_strings=False)
            bib_database = bp.parse(data)
            # for input in bib_database.entries:
            #     da = str(input.get('da'))
            #     print(da)
            count_sucess = 0
            if len(bib_database.entries) >= 10:

                for input in bib_database.entries:
                    keyo = str(input.get('keywords-plus'))
                    keys = keyo.split(';')
                    for k in keys:
                        if not 'None' in keys[0]:
                            k = k.replace("{", "")
                            k = k.replace("}", "")
                            k = k.replace(".", "")
                            k = k.replace("!", "")
                            k = k.replace("%", "")
                            k = k.replace("$", "")
                            k = k.replace("#", "")
                            k = k.replace("*", "")
                            k = k.strip()
                            k = k.replace("\n", "")
                            k = k.lower()
                            keyso = KeywordName()
                            keyso.name = str(k)
                            keyso.save()
                            count_sucess += 1

        return Response({"success": count_sucess}, status=200)



class InsertInitKeyword3(APIView):
    def get(self, request):

        folder0 = "/home/timnak/Desktop/BIBFILE"
        all_files = os.listdir("/home/timnak/Desktop/BIBFILE")
        aall_files = all_files.sort(reverse=False)
        for all in all_files:
            fpath = folder0 + "/" + all
            print(all)
            # open text file in read mode
            text_file = open(fpath, "r")

            # read whole file to a string
            data = text_file.read()

            # close file
            text_file.close()
            # print(type(data))
            bp = BibTexParser(interpolate_strings=False)
            bib_database = bp.parse(data)
            # for input in bib_database.entries:
            #     da = str(input.get('da'))
            #     print(da)
            count_sucess = 0
            if len(bib_database.entries) >= 10:

                for input in bib_database.entries:

                    keyo = str(input.get('research-areas'))

                    keys = keyo.split(';')

                    for k in keys:
                        # print(k,keys[0])
                        if not 'None' in keys[0]:
                            k = k.replace("{", "")
                            k = k.replace("}", "")
                            k = k.replace(".", "")
                            k = k.replace("!", "")
                            k = k.replace("%", "")
                            k = k.replace("$", "")
                            k = k.replace("#", "")
                            k = k.replace("*", "")
                            k = k.strip()
                            k = k.replace("\n", "")
                            k = k.lower()
                            keyso = CategoryName()
                            keyso.name = str(k)
                            keyso.save()
                            # print(keyso.name)
                            count_sucess += 1

        return Response({"success": count_sucess}, status=200)


class CategoryAlphabetList(APIView):
    def get(self, request):
        alphabet = []
        alpha = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
                 'u', 'v',
                 'w', 'x', 'y', 'z']
        for x in alpha:

            series = []
            queryset = CategoryName.objects.filter(name__startswith=x)
            serial = CategoryNameSerializer(queryset, many=True)


            if len(queryset) != 0:
                serails = (serial.data)
                # print(dict((serial.data)))
                for d in serial.data:
                    cat = ""
                    cat = str(dict(d)['name']).replace(" \&","")
                    cat = cat.replace("\\&", "")
                    series.append(cat)
            alphabet.append({x: series})

        return Response(alphabet, status=200)

class ProffesorYab(APIView):
    def get(self, request):
        count_prof=0
        prof = []
        sc = self.request.GET.get("sc")
        categories = sc.split()
        q_objects = Q()
        for cat in categories:
            q_objects &= Q(sc__icontains=cat)
        idd  = self.request.GET.get("idd")
        email= Professor.objects.filter(q_objects &  Q (idd__icontains=idd)).values_list('em')
        for em in email:
            c = str(em).count("@")
            count_prof = c+count_prof

        price = Tariff.objects.filter(
            Q(result_from__lte=count_prof) & Q(result_to__gte=count_prof) & Q(validity=True)).values('id',
                                                                                                     'title_latin',
                                                                                                     'title_farsi',
                                                                                                     'thumbnails',
                                                                                                     'price_in_IRR')
        print(price)
        return Response({"count": count_prof,'details':price}, status=200)


class CreateApplications(generics.CreateAPIView):
    queryset = Application.objects.all()
    serializer_class = ApplicationSerializer


class GotoVisachanceDargah(APIView):
    def get(self, request):
        id = self.request.GET.get('id') #id tariff
        tariff = Tariff.objects.get(pk=id)
        appnumber = self.request.GET.get('appnumber') # appnumber
        print(ENDPOINT_PAYMENT)

        url = ENDPOINT_PAYMENT+"?appnumber="+str(appnumber)+"&fee="+str(tariff.price_in_IRR)
        payload = {}
        headers = {}

        response_token = requests.request("GET", url, headers=headers, data=payload)

        print(response_token.text)
        # request to dargah fee app_number
        urltoken = "https://pna.shaparak.ir/_ipgw_/payment/"

        payload = '?amount=&resNum=&MID=&goodRefrenceId=&merchantData=&redirectURL=&lang=fa&token=' + str(
            json.loads(response_token.text)["token"])

        print(response_token.text)
        request.session["lang"] = "fa"
        request.session["token"] = str(json.loads(response_token.text)["token"])
        return HttpResponseRedirect(redirect_to=urltoken + payload)

class GotoVisachanceResultPayment(APIView):
    def get(self, request):
        token = self.request.GET.get('token')
        #  success and fail
        if token =="null":
            # fail and save result
            pass
        else:
            # save result and payment and id database
            pass

            # return Response({"count":count_prof,'details':price}, status=200)
