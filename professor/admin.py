
from django.contrib import admin

from professor.models import Professor, Scrape
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from django.apps import apps
from django.contrib import admin
from django.contrib.admin.sites import AlreadyRegistered
from .models import *


class ProfessorAdmin(admin.ModelAdmin):
    search_fields = ['ti','add','em','jo','au']
    list_display = [ 'py', 'created_at', 'em','add','fn','ti','jo','au','pt']
admin.site.register(Professor,ProfessorAdmin)

class ScrapeAdmin(admin.ModelAdmin):
    search_fields = ['journal','about','status','qid']
    list_display = [ 'journal', 'qid', 'status','count','id']
admin.site.register(Scrape,ScrapeAdmin)


class ScrapeIEEEAdmin(admin.ModelAdmin):
    search_fields = ['journal','status']
    list_display = [ 'journal','status']
admin.site.register(ScrapeIEEE,ScrapeIEEEAdmin)


class keywordNameAdmin(admin.ModelAdmin):
    search_fields = ['name']
    list_display =  ['name']
admin.site.register(KeywordName,keywordNameAdmin)
# from django.apps import apps
# from django.contrib import admin
# from django.contrib.admin.sites import AlreadyRegistered
# from import_export import resources
# from import_export.admin import ImportExportModelAdmin
# from django.apps import apps
# from django.contrib import admin
# from django.contrib.admin.sites import AlreadyRegistered
# from .models import *
# admin.site.register(ScrapeIEEE)
# admin.site.register(Professor)
# #
# # app_models = apps.get_app_config('professor').get_models()
# # for model in app_models:
# #     try:
# #         class AAAResource(resources.ModelResource):
# #             class Meta:
# #                 model = model
# #         class AAAAdmin(ImportExportModelAdmin):
# #             resource_class = AAAResource
# #         admin.site.register(model, AAAAdmin)
# #     except AlreadyRegistered:
# #         pass
# #
# #
# # app_models = apps.get_app_config('professor').get_models()
# # for model in app_models:
# #     try:
# #         class AAAResource(resources.ModelResource):
# #             class Meta:
# #                 model = model
# #         class AAAAdmin(ImportExportModelAdmin):
# #             resource_class = AAAResource
# #         admin.site.register(model, AAAAdmin)
# #     except AlreadyRegistered:
# #         pass

class ProfessorCleanedAdmin(admin.ModelAdmin):
    search_fields = ['em','sc','au']
    list_display = ['em','sc','au']
admin.site.register(ProfessorCleaned,ProfessorCleanedAdmin)


class CategoryNameAdmin(ImportExportModelAdmin):
    resource_classes = [CategoryName]
    search_fields = ['name']
    list_display = ['name']
admin.site.register(CategoryName,CategoryNameAdmin)


admin.site.register(EmailTemplate)
admin.site.register(Application)
admin.site.register(Tariff)
