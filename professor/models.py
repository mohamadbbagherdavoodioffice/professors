from django.db import models
import string
import datetime
import json

from django.db import models
import random
class Professor(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    fn = models.TextField(blank=True,help_text='fn',verbose_name="File Name")
    vr = models.TextField(blank=True,help_text='vr',verbose_name="Version Number")
    pt = models.TextField(blank=True, help_text='pt', verbose_name="Publication Type")
    # author
    au = models.TextField(blank=True, help_text='au', verbose_name="Author Full Name")
    bu = models.TextField(blank=True, help_text='bu', verbose_name="Book Authors")
    bf = models.TextField(blank=True, help_text='bf', verbose_name="Book Authors Full Name")
    ca = models.TextField(blank=True, help_text='ca', verbose_name="Group Authors")
    gp = models.TextField(blank=True, help_text='gp', verbose_name="Book Group Authors")
    be = models.TextField(blank=True, help_text='be', verbose_name="Editors")
    # Title
    ti = models.TextField(blank=True, help_text='ti', verbose_name="Document Title")
    so = models.TextField(blank=True, help_text='so', verbose_name="Publication Name")
    se = models.TextField(blank=True, help_text='se', verbose_name="Book Series Title")
    se = models.TextField(blank=True, help_text='se', verbose_name="Book Series Title")
    bs = models.TextField(blank=True, help_text='bs', verbose_name="Book Series Subtitle")
    # Affiliation
    af = models.TextField(blank=True, help_text='af', verbose_name="Affiliation")
    # Language
    la = models.TextField(blank=True, help_text='la', verbose_name="Language")
    # Type
    dt = models.TextField(blank=True, help_text='dt', verbose_name="Document Type")

    ct = models.TextField(blank=True, help_text='ct', verbose_name="Conference Title")
    cd = models.TextField(blank=True, help_text='cd', verbose_name="Conference Date")
    cl = models.TextField(blank=True, help_text='cl', verbose_name="Conference Location")
    sp = models.TextField(blank=True, help_text='sp', verbose_name="Conference Sponsors")
    ho = models.TextField(blank=True, help_text='ho', verbose_name="Conference Host")
    # Keywords
    de = models.TextField(blank=True, help_text='de', verbose_name="Author Keywords")
    idd = models.TextField(blank=True, help_text='id', verbose_name="Keywords Plus")
    ab = models.TextField(blank=True, help_text='ab', verbose_name="Abstract")
    c1 = models.TextField(blank=True, help_text='c1', verbose_name="Author Address")
    rp = models.TextField(blank=True, help_text='rp', verbose_name="Reprint Address")
    # Author-Email
    em = models.TextField(blank=True, help_text='em', verbose_name="E-mail Address")
    ri = models.TextField(blank=True, help_text='ri', verbose_name="ResearcherID Number")
    oi = models.TextField(blank=True, help_text='oi', verbose_name="ORCID Identifier (Open Researcher and Contributor ID)")
    fu = models.TextField(blank=True, help_text='fu', verbose_name="Funding Agency and Grant Number")
    fx = models.TextField(blank=True, help_text='fx', verbose_name="Funding Text")
    # Cited-References
    cr = models.TextField(blank=True, help_text='cr', verbose_name="Cited References")
    # Number-of-Cited-References
    nr = models.TextField(blank=True, help_text='nr', verbose_name="Cited Reference Count")
    # Times-Cited
    tc = models.TextField(blank=True, help_text='tc', verbose_name="Web of Science Core Collection Times Cited Count")
    z9 = models.TextField(blank=True, help_text='z9', verbose_name="Total Times Cited Count (Web of Science Core Collection, BIOSIS Citation Index, Chinese Science Citation Database, Data Citation Index, Russian Science Citation Index, SciELO Citation Index)")
    # Usage-Count-Last-180-days
    u1 = models.TextField(blank=True, help_text='u1', verbose_name="Usage Count (Last 180 Days)")
    # Usage-Count-Since-2013
    u2 = models.TextField(blank=True, help_text='u2', verbose_name="Usage Count (Since 2013)")
    # Publisher
    pu = models.TextField(blank=True, help_text='pu', verbose_name="Publisher")
    # Address
    add = models.TextField(blank=True, help_text='add', verbose_name="Address")
    pi = models.TextField(blank=True, help_text='pi', verbose_name="Publisher City")
    pa = models.TextField(blank=True, help_text='pa', verbose_name="Publisher Address")
    sn = models.TextField(blank=True, help_text='sn', verbose_name="International Standard Serial Number (ISSN)")
    # EISSN
    ei = models.TextField(blank=True, help_text='ei', verbose_name="Electronic International Standard Serial Number (eISSN)")
    bn = models.TextField(blank=True, help_text='bn', verbose_name="International Standard Book Number (ISBN)")
    j9 = models.TextField(blank=True, help_text='j9', verbose_name="29-Character Source Abbreviation")
    # Journal-ISO
    j1 = models.TextField(blank=True, help_text='j1', verbose_name="ISO Source Abbreviation")
    # DA
    pd = models.TextField(blank=True, help_text='pd', verbose_name="Publication Date")
    # year
    py = models.TextField(blank=True, help_text='py', verbose_name="Year Published")
    # Volume
    vl = models.TextField(blank=True, help_text='vl', verbose_name="Volume")
    # Number
    nu = models.TextField(blank=True, help_text='nu', verbose_name="Number")
    # Pages
    pag = models.TextField(blank=True, help_text='pag', verbose_name="pages")
    # Month
    mo = models.TextField(blank=True, help_text='mo', verbose_name="Month")
    iss = models.TextField(blank=True, help_text='is', verbose_name="Issue")
    si = models.TextField(blank=True, help_text='si', verbose_name="Special Issue")
    pn = models.TextField(blank=True, help_text='pn', verbose_name="Part Number")
    su = models.TextField(blank=True, help_text='su', verbose_name="Supplement")
    ma = models.TextField(blank=True, help_text='ma', verbose_name="Meeting Abstract")
    bp = models.TextField(blank=True, help_text='bp', verbose_name="Beginning Page")
    ep = models.TextField(blank=True, help_text='ep', verbose_name="Ending Page")
    ar = models.TextField(blank=True, help_text='ar', verbose_name="Article Number")
    # DOI
    di = models.TextField(blank=True, help_text='di', verbose_name="Digital Object Identifier (DOI)")
    d2 = models.TextField(blank=True, help_text='d2', verbose_name="Book Digital Object Identifier (DOI)")
    ea = models.TextField(blank=True, help_text='ea', verbose_name="Early access date")
    ey = models.TextField(blank=True, help_text='ey', verbose_name="Early access year")
    pg = models.TextField(blank=True, help_text='pg', verbose_name="Page Count")
    p2 = models.TextField(blank=True, help_text='p2', verbose_name="Chapter Count (Book Citation Index)")
    # Web-of-Science-Categories
    wc = models.TextField(blank=True, help_text='wc', verbose_name="Web of Science Categories")
    # Research - Areas
    sc = models.TextField(blank=True, help_text='sc', verbose_name="Research Areas")
    # Doc-Delivery-Number
    ga = models.TextField(blank=True, help_text='ga', verbose_name="Document Delivery Number")
    pm = models.TextField(blank=True, help_text='pm', verbose_name="PubMed ID")
    ut = models.TextField(blank=True, help_text='ut', verbose_name="Accession Number")
    # OA
    oa = models.TextField(blank=True, help_text='oa', verbose_name="Open Access Indicato")
    hp = models.TextField(blank=True, help_text='hp', verbose_name="ESI Hot Paper. Note that this field is valued only for ESI subscribers.")
    ha = models.TextField(blank=True, help_text='hc', verbose_name="ESI Highly Cited Paper. Note that this field is valued only for ESI subscribers.")
    da = models.TextField(blank=True, help_text='da', verbose_name="Date this report was generated")
    er = models.TextField(blank=True, help_text='er', verbose_name="End of Record")
    ef = models.TextField(blank=True, help_text='ef', verbose_name="End of File")
    # Journal
    jo = models.TextField(blank=True, help_text='ef', verbose_name="Journal")
    # Unique-ID
    ui = models.TextField(blank=True, help_text='ui', verbose_name="Unique-ID")
    # booktitle
    bo = models.TextField(blank=True, help_text='bo', verbose_name="Book Title")
    # ENTRYTYPE
    en = models.TextField(blank=True, help_text='en', verbose_name="ENTRYTYPE")
    # ENTRYTYPE
    idwos = models.TextField(blank=True, help_text='idwos', verbose_name="WOS")
    # organization
    orr = models.TextField(blank=True, help_text='orr', verbose_name="Organization")
    # note
    ntt = models.TextField(blank=True, help_text='ntt', verbose_name="Note")

    # researcherid-numbers
#
class Scrape(models.Model):
    status = models.CharField(max_length=9, choices=(
    ('qid', 'qid'), ('NOJouranl', 'NOJouranl'), ('stid', 'stid'), ('Fail', 'Fail'), ('Success', 'Success'),
    ('None', 'None')), default='None')
    stid = models.TextField(blank=True,null=True)
    qid = models.TextField(blank=True,null=True)
    count = models.IntegerField(blank=True, null=True)
    journal = models.TextField(blank=True, null=True)
    about = models.TextField(blank=True, null=True)
    year = models.IntegerField(blank=True, null=True)
    created_at = models.DateField(auto_now_add=True)
    note = models.TextField(blank=True,null=True)

class ScrapeIEEE(models.Model):
    status = models.CharField(max_length=9, choices=(
    ('qid', 'qid'), ('NOJouranl', 'NOJouranl'), ('stid', 'stid'), ('Fail', 'Fail'), ('Success', 'Success'),
    ('None', 'None')), default='None')
    # year_id = models.IntegerField(max_length=9, choices=((0,0),(1,1),(2,2),(3,3)),null=True,blank=True)
    year_name = models.IntegerField(blank=True, null=True)
    count = models.IntegerField(blank=True, null=True)
    journal = models.TextField(blank=True, null=True)
    about = models.TextField(blank=True, null=True)
    created_at = models.DateField(auto_now_add=True)
    note = models.TextField(blank=True,null=True)



class ProfessorCleaned(models.Model):
    # Research - Areas
    sc = models.TextField(blank=True, help_text='sc', verbose_name="Research Areas")
    # author
    au = models.TextField(blank=True, help_text='au', verbose_name="Author Full Name")
    # Author-Email
    em = models.TextField(blank=True, help_text='em', verbose_name="E-mail Address")
    created_at = models.DateTimeField(auto_now_add=True)


class KeywordName(models.Model):
    name = models.TextField(blank=True, help_text='idds', verbose_name="keyword", primary_key=True)

class CategoryName(models.Model):
    name = models.TextField(blank=True, help_text='idds', verbose_name="keyword", )
    def __str__(self):
        return "%s " % (self.name)


class Tariff(models.Model):

    created = models.DateField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    title_latin = models.CharField(max_length=450, null=True, blank=True)
    title_farsi = models.CharField(max_length=450, null=True, blank=True)
    thumbnails = models.ImageField(upload_to='thumbs/%Y/%m/%d/', blank=True,
                                   verbose_name='thumbnails')
    result_from = models.DecimalField(max_digits=10, decimal_places=0, null=True, blank=True)
    result_to = models.DecimalField(max_digits=10, decimal_places=0, null=True, blank=True)
    percent = models.DecimalField(max_digits=3, decimal_places=0, null=True, blank=True)
    price_in_IRR = models.DecimalField(max_digits=10, decimal_places=0, null=True, blank=True)
    validity = models.BooleanField(default=False, null=True, blank=True)
    def __str__(self):
        return "%s | %s" % (self.title_latin, self.id)
class EmailTemplate(models.Model):
    email_title = models.CharField(max_length=400, null=True, blank=True)
    email_text = models.CharField(max_length=400, null=True, blank=True)
    email_template_file = models.ImageField(upload_to='fileemail/%Y/%m/%d/', blank=True,
                                   verbose_name='fileemail')


def get_file_prof(instance, filename):
    return f"file_visa_form/{filename}"
class Application(models.Model):


    selected_category = models.ForeignKey(CategoryName, on_delete=models.CASCADE, null=True, blank=True, default=None)
    keywords = models.TextField(null=True, blank=True)
    total_results = models.CharField(max_length=200, null=True, blank=True)
    tariff = models.ForeignKey(Tariff, on_delete=models.CASCADE, null=True, blank=True, default=None)
    # amount = models.DecimalField(max_digits=10, decimal_places=1, null=True, blank=True)
    name = models.CharField(max_length=200, null=True, blank=True)
    cell = models.CharField(max_length=200, null=True, blank=True)
    email = models.CharField(max_length=200, null=True, blank=True)
    town = models.CharField(max_length=200, null=True, blank=True)
    application_number = models.CharField(max_length=200, null=True, blank=True)
    date_time = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    trace_code = models.CharField(max_length=40, null=True, blank=True)
    session_id = models.CharField(max_length=200, null=True, blank=True)
    token_id = models.CharField(max_length=200, null=True, blank=True)
    bank_detail = models.TextField(null=True, blank=True)
    bank_result = models.JSONField(null=True, blank=True)
    payment_status = models.BooleanField(default=False, null=True, blank=True)
    error = models.TextField(null=True, blank=True)
    email_sent_boolean = models.BooleanField(default=False, null=True, blank=True)
    email_sent_datetime = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    sent_file = models.FileField(upload_to=get_file_prof)
    statisfaction = models.CharField(max_length=200, null=True, blank=True)
    client_feedback = models.CharField(max_length=200, null=True, blank=True)


    def save(self, *args, **kwargs):
        months = ["A",
                  "B",
                  "C",
                  "D",
                  "E",
                  "F",
                  "G",
                  "H",
                  "I",
                  "J",
                  "K",
                  "L"]
        not_unique = True
        if self != None:
            while not_unique:
                count_user = Application.objects.count()
                count_visit = Application.objects.count()
                count_zeroes = '0' * (4 - len(str(count_user + 1)))

                rand_1 = str(random.choice(string.ascii_letters)).upper()
                rand_2 = str(random.choice(string.ascii_letters)).upper()
                not_equal = True

                while not_equal:
                    rand_1 = str(random.choice(string.ascii_letters)).upper()
                    rand_2 = str(random.choice(string.ascii_letters)).upper()
                    if rand_1 != 'O' or rand_2 != 'O' or rand_1 != rand_2:
                        not_equal = False

                    self.application_number = rand_1 + rand_2 + \
                                              str(random.randint(2, 8)) + count_zeroes + str(count_user) + str(
                        random.choice(string.ascii_letters)).upper() + \
                                              str(datetime.datetime.now().day) + str(
                        random.choice(string.ascii_letters)).upper() + str(round(count_visit / 10000) + 1)

                    if not Application.objects.filter(application_number=str(self.application_number)):
                        not_unique = False

            # if not VisaMatchPhasea.objects.filter(application_number__icontains=str(self.application_number)):
            #     not_unique = False

            super().save(*args, **kwargs)


